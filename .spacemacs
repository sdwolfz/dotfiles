;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
    ;; Base distribution to use. This is a layer contained in the directory
    ;; `+distribution'. For now available distributions are `spacemacs-base'
    ;; or `spacemacs'. (default 'spacemacs)
    dotspacemacs-distribution 'spacemacs

    ;; Lazy installation of layers (i.e. layers are installed only when a file
    ;; with a supported type is opened). Possible values are `all', `unused'
    ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
    ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
    ;; lazy install any layer that support lazy installation even the layers
    ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
    ;; installation feature and you have to explicitly list a layer in the
    ;; variable `dotspacemacs-configuration-layers' to install it.
    ;; (default 'unused)
    dotspacemacs-enable-lazy-installation 'unused

    ;; If non-nil then Spacemacs will ask for confirmation before installing
    ;; a layer lazily. (default t)
    dotspacemacs-ask-for-lazy-installation t

    ;; List of additional paths where to look for configuration layers.
    ;; Paths must have a trailing slash (i.e. "~/.mycontribs/")
    dotspacemacs-configuration-layer-path '()

    ;; List of configuration layers to load.
    dotspacemacs-configuration-layers
    '(
       ;; ----------------------------------------------------------------
       ;; Example of useful layers you may want to use right away.
       ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
       ;; `M-m f e R' (Emacs style) to install them.
       ;; ----------------------------------------------------------------
       ;; Checkers
       ;; ----------------------------------------------------------------
       (spell-checking
         :variables
         enable-flyspell-auto-completion  t
         spell-checking-enable-by-default nil)
       (syntax-checking
         :variables
         syntax-checking-enable-by-default nil)

       ;; ----------------------------------------------------------------
       ;; Completion
       ;; ----------------------------------------------------------------
       auto-completion
       ;; better-defaults
       helm
       ;; ivy

       ;; Emacs
       ;; ----------------------------------------------------------------
       (org
         :variables
         ;; Extra features
         org-enable-hugo-support      nil
         org-enable-reveal-js-support nil
         org-enable-trello-support    nil
         ;; Indentation
         org-adapt-indentation nil
         org-startup-indented  nil)
       tabs

       ;; ----------------------------------------------------------------
       ;; Filetree
       ;; ----------------------------------------------------------------
       ;; neotree
       treemacs

       ;; ----------------------------------------------------------------
       ;; Frameworks
       ;; ----------------------------------------------------------------
       ;; phoenix
       ;; react
       ruby-on-rails
       ;; (vue
       ;;   :variables
       ;;     vue-backend 'lsp)

       ;; ----------------------------------------------------------------
       ;; Fun
       ;; ----------------------------------------------------------------
       ;; selectric

       ;; ----------------------------------------------------------------
       ;; International support
       ;; ----------------------------------------------------------------
       ;; chinese

       ;; ----------------------------------------------------------------
       ;; Languages
       ;; ----------------------------------------------------------------
       ;; (c-c++
       ;;   :variables
       ;;   c-c++-backend 'lsp-ccls
       ;;   c-c++-enable-clang-format-on-save nil
       ;;   c-c++-lsp-executable "~/Workspace/Hub/ccls/Release/ccls")
       ;; (clojure
       ;;   :variables
       ;;     clojure-enable-clj-refactor nil
       ;;     clojure-enable-sayid        nil)
       ;; crystal
       csv
       ;; elixir
       emacs-lisp
       ;; erlang
       ;; (go
       ;;   :variables
       ;;     go-backend            'lsp
       ;;     go-install-after-save t)
       ;; graphviz
       ;; groovy
       ;; haskell
       html
       ;; ipython-notebook
       ;; (java
       ;;   :variables
       ;;   java-backend 'lsp)
       (javascript
         :variables
         javascript-backend 'lsp
         javascript-fmt-tool 'prettier)
       ;; julia
       markdown
       ;; pact
       ;; (php
       ;;   :variables
       ;;     php-backend 'lsp)
       ;; python
       (ruby
         :variables
         ruby-backend              'lsp
         ruby-language-backend     'lsp
         ruby-syntax-backend       'tree-sitter
         ruby-enable-enh-ruby-mode nil
         ruby-version-manager      'rvm)
       ;; (rust
       ;;   :variables
       ;;   rust-backend 'lsp)
       shell-scripts
       (sql
         :variables
         sql-auto-indent nil)
       toml
       ;; (typescript
       ;;   :variables
       ;;     typescript-backend 'lsp)
       ;; vimscript
       ;; windows-scripts
       yaml

       ;; ----------------------------------------------------------------
       ;; Misc
       ;; ----------------------------------------------------------------
       ;; copy-as-format
       multiple-cursors

       ;; ----------------------------------------------------------------
       ;; OS
       ;; ----------------------------------------------------------------
       ;; nixos

       ;; ----------------------------------------------------------------
       ;; Source Controll
       ;; ----------------------------------------------------------------
       git

       ;; ----------------------------------------------------------------
       ;; Tools
       ;; ----------------------------------------------------------------
       ansible
       ;; dap
       docker
       ;; eglot
       (lsp
         :variables
         lsp-file-watch-threshold           nil
         lsp-headerline-arrow               ">"
         lsp-headerline-breadcrumb-enable   t
         lsp-headerline-breadcrumb-segments '(file symbols)
         lsp-idle-delay                     0.500
         lsp-lens-enable                    t
         lsp-response-timeout               30
         lsp-restart                        'ignore
         )
       ;; meson
       nginx
       (node
         :variables
         node-add-modules-path t)
       ;; pass
       restclient
       shell
       systemd
       (terraform
         :variables
         lsp-terraform-server '("terraform-ls" "serve"))
       (tree-sitter
         :variables
         tree-sitter-fold-enable             t
         tree-sitter-indent-enable           t
         tree-sitter-syntax-highlight-enable t)

       ;; ----------------------------------------------------------------
       ;; Web services
       ;; ----------------------------------------------------------------
       ;; search-engine
       llm-client

       )
    ;; List of additional packages that will be installed without being wrapped
    ;; in a layer (generally the packages are installed only and should still be
    ;; loaded using load/require/use-package in the user-config section below in
    ;; this file). If you need some configuration for these packages, then
    ;; consider creating a layer. You can also put the configuration in
    ;; `dotspacemacs/user-config'. To use a local version of a package, use the
    ;; `:location' property: '(your-package :location "~/path/to/your-package/")
    ;; Also include the dependencies as they will not be resolved automatically.
    dotspacemacs-additional-packages '(
                                        ;; bind-map
                                        ;;
                                        ;; (bind-map :location
                                        ;;             (recipe :fetcher file
                                        ;;                     :path "~/Workspace/Hub/emacs-bind-map")
                                        ;;           :update nil)

                                        ;; Monokai
                                        ;;
                                        ;; (monokai-theme :location
                                        ;;             (recipe :fetcher file
                                        ;;                     :path "~/Workspace/Hub/monokai-emacs")
                                        ;;           :update nil)

                                        ;; Treemacs
                                        ;;
                                        ;; (treemacs :location
                                        ;;             (recipe :fetcher file
                                        ;;                     :path "~/Workspace/Hub/treemacs/src/elisp")
                                        ;;           :update nil)

                                        ;; More color themes
                                        doom-themes

                                        ;; The future of indentation
                                        indent-bars

                                        ;; Clipboard interaction
                                        xclip)

    ;; A list of packages that cannot be updated.
    dotspacemacs-frozen-packages '()

    ;; A list of packages that will not be installed and loaded.
    dotspacemacs-excluded-packages '(
                                      ;; Icons are not useful in the terminal
                                      all-the-icons

                                      ;; Press `fd' to simulate `ESC'. Anoying when typing
                                      evil-escape

                                      ;; Poor performance LSP like for Ruby
                                      robe

                                      ;; Ugly sized bottom line
                                      spaceline
                                      spaceline-all-the-icons
                                      )

    ;; Defines the behaviour of Spacemacs when installing packages.
    ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
    ;; `used-only' installs only explicitly used packages and deletes any unused
    ;; packages as well as their unused dependencies. `used-but-keep-unused'
    ;; installs only the used packages but won't delete unused ones. `all'
    ;; installs *all* packages supported by Spacemacs and never uninstalls them.
    ;; (default is `used-only')
    dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
    ;; If non-nil then enable support for the portable dumper. You'll need to
    ;; compile Emacs 27 from source following the instructions in file
    ;; EXPERIMENTAL.org at to root of the git repository.
    ;;
    ;; WARNING: pdumper does not work with Native Compilation, so it's disabled
    ;; regardless of the following setting when native compilation is in effect.
    ;;
    ;; (default nil)
    dotspacemacs-enable-emacs-pdumper nil

    ;; Name of executable file pointing to emacs 27+. This executable must be
    ;; in your PATH.
    ;; (default "emacs")
    dotspacemacs-emacs-pdumper-executable-file "emacs"

    ;; Name of the Spacemacs dump file. This is the file will be created by the
    ;; portable dumper in the cache directory under dumps sub-directory.
    ;; To load it when starting Emacs add the parameter `--dump-file'
    ;; when invoking Emacs 27.1 executable on the command line, for instance:
    ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
    ;; (default (format "spacemacs-%s.pdmp" emacs-version))
    dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

    ;; Maximum allowed time in seconds to contact an ELPA repository.
    ;; (default 5)
    dotspacemacs-elpa-timeout 5

    ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
    ;; This is an advanced option and should not be changed unless you suspect
    ;; performance issues due to garbage collection operations.
    ;; (default '(100000000 0.1))
    dotspacemacs-gc-cons '(100000000 0.1)

    ;; Set `read-process-output-max' when startup finishes.
    ;; This defines how much data is read from a foreign process.
    ;; Setting this >= 1 MB should increase performance for lsp servers
    ;; in emacs 27.
    ;; (default (* 1024 1024))
    dotspacemacs-read-process-output-max (* 1024 1024)

    ;; If non-nil then Spacelpa repository is the primary source to install
    ;; a locked version of packages. If nil then Spacemacs will install the
    ;; latest version of packages from MELPA. Spacelpa is currently in
    ;; experimental state please use only for testing purposes.
    ;; (default nil)
    dotspacemacs-use-spacelpa nil

    ;; If non-nil then verify the signature for downloaded Spacelpa archives.
    ;; (default t)
    dotspacemacs-verify-spacelpa-archives t

    ;; If non-nil then spacemacs will check for updates at startup
    ;; when the current branch is not `develop'. Note that checking for
    ;; new versions works via git commands, thus it calls GitHub services
    ;; whenever you start Emacs. (default nil)
    dotspacemacs-check-for-update nil

    ;; If non-nil, a form that evaluates to a package directory. For example, to
    ;; use different package directories for different Emacs versions, set this
    ;; to `emacs-version'. (default 'emacs-version)
    dotspacemacs-elpa-subdirectory 'emacs-version

    ;; One of `vim', `emacs' or `hybrid'.
    ;; `hybrid' is like `vim' except that `insert state' is replaced by the
    ;; `hybrid state' with `emacs' key bindings. The value can also be a list
    ;; with `:variables' keyword (similar to layers). Check the editing styles
    ;; section of the documentation for details on available variables.
    ;; (default 'vim)
    dotspacemacs-editing-style '(vim
                                  :variables
                                  vim-style-remap-Y-to-y$ t)

    ;; If non-nil show the version string in the Spacemacs buffer. It will
    ;; appear as (spacemacs version)@(emacs version)
    ;; (default t)
    dotspacemacs-startup-buffer-show-version t

    ;; Specify the startup banner. Default value is `official', it displays
    ;; the official spacemacs logo. An integer value is the index of text
    ;; banner, `random' chooses a random text banner in `core/banners'
    ;; directory. A string value must be a path to an image format supported
    ;; by your Emacs build.
    ;; If the value is nil then no banner is displayed. (default 'official)
    dotspacemacs-startup-banner 'official

    ;; Scale factor controls the scaling (size) of the startup banner. Default
    ;; value is `auto' for scaling the logo automatically to fit all buffer
    ;; contents, to a maximum of the full image height and a minimum of 3 line
    ;; heights. If set to a number (int or float) it is used as a constant
    ;; scaling factor for the default logo size.
    dotspacemacs-startup-banner-scale 'auto

    ;; List of items to show in startup buffer or an association list of
    ;; the form `(list-type . list-size)`. If nil then it is disabled.
    ;; Possible values for list-type are:
    ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
    ;; List sizes may be nil, in which case
    ;; `spacemacs-buffer-startup-lists-length' takes effect.
    ;; The exceptional case is `recents-by-project', where list-type must be a
    ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
    ;; number is the project limit and the second the limit on the recent files
    ;; within a project.
    dotspacemacs-startup-lists '((recents . 7)
                                  (projects . 7))

    ;; True if the home buffer should respond to resize events. (default t)
    dotspacemacs-startup-buffer-responsive t

    ;; Show numbers before the startup list lines. (default t)
    dotspacemacs-show-startup-list-numbers t

    ;; The minimum delay in seconds between number key presses. (default 0.4)
    dotspacemacs-startup-buffer-multi-digit-delay 0.4

    ;; If non-nil, show file icons for entries and headings on Spacemacs home buffer.
    ;; This has no effect in terminal or if "all-the-icons" package or the font
    ;; is not installed. (default nil)
    dotspacemacs-startup-buffer-show-icons nil

    ;; Default major mode for a new empty buffer. Possible values are mode
    ;; names such as `text-mode'; and `nil' to use Fundamental mode.
    ;; (default `text-mode')
    dotspacemacs-new-empty-buffer-major-mode 'text-mode

    ;; Default major mode of the scratch buffer (default `text-mode')
    dotspacemacs-scratch-mode 'text-mode

    ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
    ;; *scratch* buffer will be saved and restored automatically.
    dotspacemacs-scratch-buffer-persistent nil

    ;; If non-nil, `kill-buffer' on *scratch* buffer
    ;; will bury it instead of killing.
    dotspacemacs-scratch-buffer-unkillable nil

    ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
    ;; (default nil)
    dotspacemacs-initial-scratch-message nil

    ;; List of themes, the first of the list is loaded when spacemacs starts.
    ;; Press `SPC T n' to cycle to the next theme in the list (works great
    ;; with 2 themes variants, one dark and one light). A theme from external
    ;; package can be defined with `:package', or a theme can be defined with
    ;; `:location' to download the theme package, refer the themes section in
    ;; DOCUMENTATION.org for the full theme specifications.
    dotspacemacs-themes '(;; doom-monokai-classic
                           ;; doom-monokai-pro
                           monokai
                           spacemacs-dark
                           spacemacs-light)

    ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
    ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
    ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
    ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
    ;; refer to the DOCUMENTATION.org for more info on how to create your own
    ;; spaceline theme. Value can be a symbol or list with additional properties.
    ;; (default '(spacemacs :separator wave :separator-scale 1.5))
    dotspacemacs-mode-line-theme '(doom)

    ;; If non-nil the cursor color matches the state color in GUI Emacs.
    ;; (default t)
    dotspacemacs-colorize-cursor-according-to-state t

    ;; Default font or prioritized list of fonts. This setting has no effect when
    ;; running Emacs in terminal. The font set here will be used for default and
    ;; fixed-pitch faces. The `:size' can be specified as
    ;; a non-negative integer (pixel size), or a floating-point (point size).
    ;; Point size is recommended, because it's device independent. (default 10.0)
    dotspacemacs-default-font '("Source Code Pro"
                                 :size 10.0
                                 :weight normal
                                 :width normal)

    ;; The leader key (default "SPC")
    dotspacemacs-leader-key "SPC"

    ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
    ;; (default "SPC")
    dotspacemacs-emacs-command-key "SPC"

    ;; The key used for Vim Ex commands (default ":")
    dotspacemacs-ex-command-key ":"

    ;; The leader key accessible in `emacs state' and `insert state'
    ;; (default "M-m")
    dotspacemacs-emacs-leader-key "M-m"

    ;; Major mode leader key is a shortcut key which is the equivalent of
    ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
    dotspacemacs-major-mode-leader-key ","

    ;; Major mode leader key accessible in `emacs state' and `insert state'.
    ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
    ;; Thus M-RET should work as leader key in both GUI and terminal modes.
    ;; C-M-m also should work in terminal mode, but not in GUI mode.
    dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

    ;; These variables control whether separate commands are bound in the GUI to
    ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
    ;; Setting it to a non-nil value, allows for separate commands under `C-i'
    ;; and TAB or `C-m' and `RET'.
    ;; In the terminal, these pairs are generally indistinguishable, so this only
    ;; works in the GUI. (default nil)
    dotspacemacs-distinguish-gui-tab nil

    ;; Name of the default layout (default "Default")
    dotspacemacs-default-layout-name "Default"

    ;; If non-nil the default layout name is displayed in the mode-line.
    ;; (default nil)
    dotspacemacs-display-default-layout nil

    ;; If non-nil then the last auto saved layouts are resumed automatically upon
    ;; start. (default nil)
    dotspacemacs-auto-resume-layouts nil

    ;; If non-nil, auto-generate layout name when creating new layouts. Only has
    ;; effect when using the "jump to layout by number" commands. (default nil)
    dotspacemacs-auto-generate-layout-names nil

    ;; Size (in MB) above which spacemacs will prompt to open the large file
    ;; literally to avoid performance issues. Opening a file literally means that
    ;; no major mode or minor modes are active. (default is 1)
    dotspacemacs-large-file-size 1

    ;; Location where to auto-save files. Possible values are `original' to
    ;; auto-save the file in-place, `cache' to auto-save the file to another
    ;; file stored in the cache directory and `nil' to disable auto-saving.
    ;; (default 'cache)
    dotspacemacs-auto-save-file-location 'cache

    ;; Maximum number of rollback slots to keep in the cache. (default 5)
    dotspacemacs-max-rollback-slots 5

    ;; If non-nil, the paste transient-state is enabled. While enabled, after you
    ;; paste something, pressing `C-j' and `C-k' several times cycles through the
    ;; elements in the `kill-ring'. (default nil)
    dotspacemacs-enable-paste-transient-state t

    ;; Which-key delay in seconds. The which-key buffer is the popup listing
    ;; the commands bound to the current keystroke sequence. (default 0.4)
    dotspacemacs-which-key-delay 0.4

    ;; Which-key frame position. Possible values are `right', `bottom' and
    ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
    ;; right; if there is insufficient space it displays it at the bottom.
    ;; It is also possible to use a posframe with the following cons cell
    ;; `(posframe . position)' where position can be one of `center',
    ;; `top-center', `bottom-center', `top-left-corner', `top-right-corner',
    ;; `top-right-corner', `bottom-left-corner' or `bottom-right-corner'
    ;; (default 'bottom)
    dotspacemacs-which-key-position 'bottom

    ;; Control where `switch-to-buffer' displays the buffer. If nil,
    ;; `switch-to-buffer' displays the buffer in the current window even if
    ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
    ;; displays the buffer in a same-purpose window even if the buffer can be
    ;; displayed in the current window. (default nil)
    dotspacemacs-switch-to-buffer-prefers-purpose nil

    ;; Whether side windows (such as those created by treemacs or neotree)
    ;; are kept or minimized by `spacemacs/toggle-maximize-window' (SPC w m).
    ;; (default t)
    dotspacemacs-maximize-window-keep-side-windows t

    ;; If nil, no load-hints enabled. If t, enable the `load-hints' which will
    ;; put the most likely path on the top of `load-path' to reduce walking
    ;; through the whole `load-path'. It's an experimental feature to speedup
    ;; Spacemacs on Windows. Refer the FAQ.org "load-hints" session for details.
    dotspacemacs-enable-load-hints nil

    ;; If t, enable the `package-quickstart' feature to avoid full package
    ;; loading, otherwise no `package-quickstart' attemption (default nil).
    ;; Refer the FAQ.org "package-quickstart" section for details.
    dotspacemacs-enable-package-quickstart nil

    ;; If non-nil a progress bar is displayed when spacemacs is loading. This
    ;; may increase the boot time on some systems and emacs builds, set it to
    ;; nil to boost the loading time. (default t)
    dotspacemacs-loading-progress-bar t

    ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
    ;; (Emacs 24.4+ only)
    dotspacemacs-fullscreen-at-startup t

    ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
    ;; Use to disable fullscreen animations in OSX. (default nil)
    dotspacemacs-fullscreen-use-non-native nil

    ;; If non-nil the frame is maximized when Emacs starts up.
    ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
    ;; (default t) (Emacs 24.4+ only)
    dotspacemacs-maximized-at-startup t

    ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
    ;; variable with `dotspacemacs-maximized-at-startup' to obtain fullscreen
    ;; without external boxes. Also disables the internal border. (default nil)
    dotspacemacs-undecorated-at-startup nil

    ;; A value from the range (0..100), in increasing opacity, which describes
    ;; the transparency level of a frame when it's active or selected.
    ;; Transparency can be toggled through `toggle-transparency'. (default 90)
    dotspacemacs-active-transparency 90

    ;; A value from the range (0..100), in increasing opacity, which describes
    ;; the transparency level of a frame when it's inactive or deselected.
    ;; Transparency can be toggled through `toggle-transparency'. (default 90)
    dotspacemacs-inactive-transparency 90

    ;; A value from the range (0..100), in increasing opacity, which describes the
    ;; transparency level of a frame background when it's active or selected. Transparency
    ;; can be toggled through `toggle-background-transparency'. (default 90)
    dotspacemacs-background-transparency 90

    ;; If non-nil show the titles of transient states. (default t)
    dotspacemacs-show-transient-state-title t

    ;; If non-nil show the color guide hint for transient state keys. (default t)
    dotspacemacs-show-transient-state-color-guide t

    ;; If non-nil unicode symbols are displayed in the mode line.
    ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
    ;; the value to quoted `display-graphic-p'. (default t)
    dotspacemacs-mode-line-unicode-symbols nil

    ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
    ;; scrolling overrides the default behavior of Emacs which recenters point
    ;; when it reaches the top or bottom of the screen. (default t)
    dotspacemacs-smooth-scrolling t

    ;; Show the scroll bar while scrolling. The auto hide time can be configured
    ;; by setting this variable to a number. (default t)
    dotspacemacs-scroll-bar-while-scrolling t

    ;; Control line numbers activation.
    ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
    ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
    ;; numbers are relative. If set to `visual', line numbers are also relative,
    ;; but only visual lines are counted. For example, folded lines will not be
    ;; counted and wrapped lines are counted as multiple lines.
    ;; This variable can also be set to a property list for finer control:
    ;; '(:relative nil
    ;;   :visual nil
    ;;   :disabled-for-modes dired-mode
    ;;                       doc-view-mode
    ;;                       markdown-mode
    ;;                       org-mode
    ;;                       pdf-view-mode
    ;;                       text-mode
    ;;   :size-limit-kb 1000)
    ;; When used in a plist, `visual' takes precedence over `relative'.
    ;; (default nil)
    dotspacemacs-line-numbers t

    ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
    ;; (default 'evil)
    dotspacemacs-folding-method 'origami

    ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
    ;; `smartparens-strict-mode' will be enabled in programming modes.
    ;; (default nil)
    dotspacemacs-smartparens-strict-mode nil

    ;; If non-nil smartparens-mode will be enabled in programming modes.
    ;; (default t)
    dotspacemacs-activate-smartparens-mode t

    ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
    ;; over any automatically added closing parenthesis, bracket, quote, etc...
    ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
    dotspacemacs-smart-closing-parenthesis nil

    ;; Select a scope to highlight delimiters. Possible values are `any',
    ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
    ;; emphasis the current one). (default 'all)
    dotspacemacs-highlight-delimiters 'all

    ;; If non-nil, start an Emacs server if one is not already running.
    ;; (default nil)
    dotspacemacs-enable-server t

    ;; Set the emacs server socket location.
    ;; If nil, uses whatever the Emacs default is, otherwise a directory path
    ;; like \"~/.emacs.d/server\". It has no effect if
    ;; `dotspacemacs-enable-server' is nil.
    ;; (default nil)
    dotspacemacs-server-socket-dir nil

    ;; If non-nil, advise quit functions to keep server open when quitting.
    ;; (default nil)
    dotspacemacs-persistent-server nil

    ;; List of search tool executable names. Spacemacs uses the first installed
    ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
    ;; (default '("rg" "ag" "pt" "ack" "grep"))
    dotspacemacs-search-tools '("ag" "rg" "pt" "ack" "grep")

    ;; The backend used for undo/redo functionality. Possible values are
    ;; `undo-fu', `undo-redo' and `undo-tree' see also `evil-undo-system'.
    ;; Note that saved undo history does not get transferred when changing
    ;; your undo system. The default is currently `undo-fu' as `undo-tree'
    ;; is not maintained anymore and `undo-redo' is very basic."
    dotspacemacs-undo-system 'undo-fu

    ;; Format specification for setting the frame title.
    ;; %a - the `abbreviated-file-name', or `buffer-name'
    ;; %t - `projectile-project-name'
    ;; %I - `invocation-name'
    ;; %S - `system-name'
    ;; %U - contents of $USER
    ;; %b - buffer name
    ;; %f - visited file name
    ;; %F - frame name
    ;; %s - process status
    ;; %p - percent of buffer above top of window, or Top, Bot or All
    ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
    ;; %m - mode name
    ;; %n - Narrow if appropriate
    ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
    ;; %Z - like %z, but including the end-of-line format
    ;; If nil then Spacemacs uses default `frame-title-format' to avoid
    ;; performance issues, instead of calculating the frame title by
    ;; `spacemacs/title-prepare' all the time.
    ;; (default "%I@%S")
    dotspacemacs-frame-title-format "%I@%S"

    ;; Format specification for setting the icon title format
    ;; (default nil - same as frame-title-format)
    dotspacemacs-icon-title-format nil

    ;; Color highlight trailing whitespace in all prog-mode and text-mode derived
    ;; modes such as c++-mode, python-mode, emacs-lisp, html-mode, rst-mode etc.
    ;; (default t)
    dotspacemacs-show-trailing-whitespace t

    ;; Delete whitespace while saving buffer. Possible values are `all'
    ;; to aggressively delete empty line and long sequences of whitespace,
    ;; `trailing' to delete only the whitespace at end of lines, `changed' to
    ;; delete only whitespace for changed lines or `nil' to disable cleanup.
    ;; The variable `global-spacemacs-whitespace-cleanup-modes' controls
    ;; which major modes have whitespace cleanup enabled or disabled
    ;; by default.
    ;; (default nil)
    dotspacemacs-whitespace-cleanup 'changed

    ;; If non-nil activate `clean-aindent-mode' which tries to correct
    ;; virtual indentation of simple modes. This can interfere with mode specific
    ;; indent handling like has been reported for `go-mode'.
    ;; If it does deactivate it here.
    ;; (default t)
    dotspacemacs-use-clean-aindent-mode t

    ;; Accept SPC as y for prompts if non-nil. (default nil)
    dotspacemacs-use-SPC-as-y nil

    ;; If non-nil shift your number row to match the entered keyboard layout
    ;; (only in insert state). Currently supported keyboard layouts are:
    ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
    ;; New layouts can be added in `spacemacs-editing' layer.
    ;; (default nil)
    dotspacemacs-swap-number-row nil

    ;; Either nil or a number of seconds. If non-nil zone out after the specified
    ;; number of seconds. (default nil)
    dotspacemacs-zone-out-when-idle nil

    ;; Run `spacemacs/prettify-org-buffer' when
    ;; visiting README.org files of Spacemacs.
    ;; (default nil)
    dotspacemacs-pretty-docs nil

    ;; If nil the home buffer shows the full path of agenda items
    ;; and todos. If non-nil only the file name is shown.
    dotspacemacs-home-shorten-agenda-source nil

    ;; If non-nil then byte-compile some of Spacemacs files.
    dotspacemacs-byte-compile t))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env)
  )

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  ;; ----------------------------------------------------------------------------
  ;; Error verbosity.
  (setq warning-minimum-level :error)

  ;; ----------------------------------------------------------------------------
  ;; Custom settings file.
  ;; https://github.com/syl20bnr/spacemacs/issues/7891
  ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html
  ;; TODO: implement it as part of core, namespaced by branch and emacs version.
  ;; ----------------------------------------------------------------------------
  (setq custom-file "~/.emacs.d/.cache/.custom-settings")
  (load custom-file)

  ;;-----------------------------------------------------------------------------
  ;; VTerm
  ;;-----------------------------------------------------------------------------
  (setq vterm-always-compile-module t)

  ;;-----------------------------------------------------------------------------
  ;; Settings
  ;;-----------------------------------------------------------------------------

  ;; Truncate lines by default.
  (setq
    truncate-partial-width-windows nil)

  (setq-default
    truncate-lines t)

  (setq safe-local-variable-values '((byte-compile . t)))

  ;;-----------------------------------------------------------------------------
  ;; Tabs
  ;;-----------------------------------------------------------------------------
  (setq centaur-tabs-enable-key-bindings t)
  (setq tabs-group-by-project nil)

  ;; Theme background transaprency.
  ;; (setq frame-resize-pixelwise t)

  ;; ----------------------------------------------------------------------------
  (push '("melpa-stable" . "stable.melpa.org/packages/") configuration-layer-elpa-archives)
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  (spacemacs/dump-modes '(org-mode markdown-mode gfm-mode))
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  ;;-----------------------------------------------------------------------------
  ;; Quick Fixes
  ;;-----------------------------------------------------------------------------
  ;; Anoying prompt for `TAGS' file.
  ;; (setq spacemacs-default-jump-handlers
  ;;   (remove 'evil-goto-definition spacemacs-default-jump-handlers))

  ;;-----------------------------------------------------------------------------
  ;; Emacs
  ;;-----------------------------------------------------------------------------
  ;; Enable scroll with mouse inside a terminal.
  (unless window-system
    (global-set-key (kbd "<mouse-4>") 'scroll-down-line)
    (global-set-key (kbd "<mouse-5>") 'scroll-up-line))

  ;; Disable lockfiles (those pesky .# symlinks).
  (setq create-lockfiles nil)

  ;; Jump over underscores like the real vim.
  ;; NOTE: The emacs way is actually more useful!
  ;; (add-hook 'prog-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))

  ;; Prefer horizontal split.
  (setq split-height-threshold 0)
  (setq split-width-threshold nil)

  ;; Don't move files to trash, permanently delete.
  ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=48280
  (setq delete-by-moving-to-trash nil)

  ;; Better vertical border character.
  (set-display-table-slot standard-display-table 'vertical-border ?│)

  ;;-----------------------------------------------------------------------------
  ;; Spacemacs
  ;;-----------------------------------------------------------------------------

  ;; Use `1 2 3 4...' instead of `a s d f...' for `ace-window' numbering.
  (setq aw-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9 ?0))

  ;; Line numbers.
  (setq-default display-line-numbers-width nil)

  ;; Margin for scrolling top and bottom.
  (setq scroll-margin 5)

  ;; General indentation.
  (setq
    tab-width       2
    standard-indent 2)

  ;; Display indentation guides with a delay for performance.
  (setq
    indent-guide-delay     0.1
    indent-guide-recursive t)

  ;; Hides highlight on current line.
  (spacemacs/toggle-highlight-current-line-globally-off)

  ;; Display indentation guides in all buffers.
  (spacemacs/toggle-indent-guide-globally-on)

  ;; TODO: Display carriage return in whitespace-mode
  ;; (with-eval-after-load 'whitespace-mode
  ;;   (add-to-list 'whitespace-display-mappings '(newline-mark ?\r [?↵])))

  ;; Don't color long lines in whitespace-mode
  (setq whitespace-style
    '(face
       tabs spaces trailing space-before-tab newline
       indentation empty space-after-tab
       space-mark tab-mark newline-mark))

  ;; Does not activate indentation guides in Spacemacs buffer.
  ;; TODO: find out why indentation guide breaks there.
  (add-to-list 'indent-guide-inhibit-modes 'spacemacs-buffer-mode)

  ;; Hides smartparens.
  (show-smartparens-global-mode -1)

  ;; Low delay on matching parens.
  (setq show-paren-delay 0.1)

  ;; Better faces for mathcing parens.
  (set-face-attribute 'show-paren-match nil :background "#FF0000")
  (set-face-attribute 'show-paren-match nil :foreground "#1D1F21")
  (set-face-attribute 'show-paren-match nil :underline  t        )

  ;; Enables parens mode.
  (show-paren-mode t)

  ;; Disables electric indent.
  (electric-indent-mode -1)

  ;; Always follow symlinks.
  (setq vc-follow-symlinks t)

  ;; Show correct column numbers in modeline indicator.
  ;; NOTE: column number functions will still report the result starting from 0.
  (setq column-number-indicator-zero-based nil)

  ;; TODO: propose this in core.
  ;; Uses `M-.` as jump to definition.
  (define-key evil-normal-state-map (kbd "M-.") 'spacemacs/jump-to-definition)

  ;;-----------------------------------------------------------------------------
  ;; Doom
  ;;-----------------------------------------------------------------------------
  (setq
    doom-modeline-column-zero-based nil
    doom-modeline-percent-position  nil)

  ;;-----------------------------------------------------------------------------
  ;; Evil
  ;;-----------------------------------------------------------------------------
  ;; General indentation
  (setq evil-shift-width 2)

  ;; Better window sizes when creating splits.
  ;; TODO: fix this in core.
  (defun my-evil-window-split-and-move ()
    (interactive)
    (evil-window-split)
    (windmove-down))
  (defun my-evil-window-vsplit-and-move ()
    (interactive)
    (evil-window-vsplit)
    (windmove-right))

  (spacemacs/set-leader-keys "ws" 'evil-window-split             )
  (spacemacs/set-leader-keys "wv" 'evil-window-vsplit            )
  (spacemacs/set-leader-keys "wS" 'my-evil-window-split-and-move )
  (spacemacs/set-leader-keys "wV" 'my-evil-window-vsplit-and-move)

  (global-set-key (kbd "ESC <up>") 'evil-force-normal-state)
  (global-set-key (kbd "ESC <down>") 'evil-force-normal-state)

  ;; Saner deletion of characters in insert mode.
  (define-key evil-insert-state-map "\C-d" 'evil-delete-char         )
  (define-key evil-insert-state-map "\C-h" 'evil-delete-backward-char)

  ;; Use the transient state and stay in place when searching thing at point.
  (define-key evil-normal-state-map (kbd "*") 'spacemacs/symbol-highlight)

  (with-eval-after-load 'evil
    (advice-add 'evil-line-move     :filter-args (lambda (args) (list (or (car args) 1) t)))
    (advice-add 'evil-forward-char  :filter-args (lambda (args) (append (butlast args) '(t))))
    (advice-add 'evil-backward-char :filter-args (lambda (args) (append (butlast args) '(t)))))

  ;;-----------------------------------------------------------------------------
  ;; Helm
  ;;-----------------------------------------------------------------------------
  (setq helm-truncate-lines t)

  ;;----------------------------------------------------------------------------
  ;; Git
  ;;----------------------------------------------------------------------------
  (setq magit-gh-pulls-pull-detail-limit 1000)

  ;;-----------------------------------------------------------------------------
  ;; Treemacs
  ;;-----------------------------------------------------------------------------
  ;; Local development
  (require 'treemacs)

  (setq
    treemacs-collapse-dirs          0
    treemacs-file-event-delay       1000
    treemacs-indentation            2
    treemacs-indentation-string     " "
    treemacs-move-forward-on-expand t
    treemacs-no-png-images          t
    treemacs-wrap-around            nil)

  (with-eval-after-load 'treemacs
    (treemacs-define-TAB-action 'file-node-open   'treemacs-visit-node-default)
    (treemacs-define-TAB-action 'file-node-closed 'treemacs-visit-node-default)

    (evil-define-key 'treemacs treemacs-mode-map (kbd "M") #'treemacs-mark-or-unmark-path-at-point)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "N") #'treemacs-reset-marks)

    ;; -------------------------------------------------------------------------

    (defun treemacs-create-file-or-dir ()
      "Interactively create either a file or directory."
      (interactive)
      (let* ((curr-path (--if-let (treemacs-current-button)
                          (treemacs--nearest-path it)
                          (expand-file-name "~")))
              (path-to-create (read-file-name
                                "Create File or Directory: "
                                (treemacs--add-trailing-slash
                                  (if (file-directory-p curr-path)
                                    curr-path
                                    (treemacs--parent-dir curr-path))))))
        (treemacs-block
          (treemacs-error-return-if (file-exists-p path-to-create)
            "%s already exists." (propertize path-to-create 'face 'font-lock-string-face))
          (treemacs--without-filewatch
            (if (not (equal path-to-create (file-name-as-directory path-to-create)))
              (-let [dir (treemacs--parent-dir path-to-create)]
                (unless (file-exists-p dir)
                  (make-directory dir t))
                (write-region "" nil path-to-create nil 0))
              (progn
                (setq path-to-create (directory-file-name path-to-create))
                (make-directory path-to-create t)))
            (run-hook-with-args 'treemacs-create-file-functions path-to-create))
          (message "%S" path-to-create)
          (-when-let (project (treemacs--find-project-for-path path-to-create))
            (-when-let* ((created-under (treemacs--parent path-to-create))
                          (created-under-pos (treemacs-find-visible-node created-under)))
              ;; update only the part that changed to keep things smooth
              ;; for files that's just their parent, for directories we have to take
              ;; flattening into account
              (if (and (treemacs-button-get created-under-pos :parent)
                    (or (treemacs-button-get created-under-pos :collapsed)
                      ;; count includes "." "..", so it'll be flattened
                      (= 3 (length (directory-files created-under)))))
                (treemacs-do-update-node (-> created-under-pos
                                           (treemacs-button-get :parent)
                                           (treemacs-button-get :path)))
                (treemacs-do-update-node created-under)))
            (treemacs-goto-file-node (treemacs-canonical-path path-to-create) project)
            (recenter))
          (treemacs-pulse-on-success
            "Created %s." (propertize path-to-create 'face 'font-lock-string-face)))))

    (evil-define-key 'treemacs treemacs-mode-map (kbd "ca") #'treemacs-create-file-or-dir)

    ;; -------------------------------------------------------------------------

    (evil-define-key 'treemacs treemacs-mode-map (kbd "ov") #'treemacs-visit-node-horizontal-split)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "oh") #'treemacs-visit-node-vertical-split)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "os") #'treemacs-visit-node-vertical-split)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "oav") #'treemacs-visit-node-ace-horizontal-split)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "oah") #'treemacs-visit-node-ace-vertical-split)
    (evil-define-key 'treemacs treemacs-mode-map (kbd "oas") #'treemacs-visit-node-ace-vertical-split)

    (evil-define-key 'treemacs treemacs-mode-map (kbd "'") #'treemacs-peek))

  ;;-----------------------------------------------------------------------------
  ;; Powerline
  ;;-----------------------------------------------------------------------------
  (unless (display-graphic-p)
    (setq powerline-default-separator 'utf-8))

  ;;-----------------------------------------------------------------------------
  ;; Conf
  ;;-----------------------------------------------------------------------------
  (add-to-list
    'auto-mode-alist
    '("\\.ini.\\(erb\\|j2\\|template\\|tpl\\|sample\\)\\'" . conf-mode))

  ;;-----------------------------------------------------------------------------
  ;; Docker
  ;;-----------------------------------------------------------------------------
  (with-eval-after-load 'dockerfile-mode
    (setq dockerfile-enable-auto-indent nil))

  (add-to-list
    'auto-mode-alist
    '("Dockerfile.\\(erb\\|j2\\|template\\|tpl\\)\\'" . dockerfile-mode))

  ;;-----------------------------------------------------------------------------
  ;; Elixir
  ;;-----------------------------------------------------------------------------
  ;; Location of source files for elixir and erlang.
  ;; (setq
  ;;   alchemist-goto-erlang-source-dir "~/Sources/otp"
  ;;   alchemist-goto-elixir-source-dir "~/Sources/elixir")

  ;; Compile project on save to update alchemist server with code changes.
  ;; (setq alchemist-hooks-compile-on-save t)

  ;;-----------------------------------------------------------------------------
  ;; Lisp
  ;;-----------------------------------------------------------------------------
  ;; Indentation
  (setq lisp-indent-offset 2)

  ;;-----------------------------------------------------------------------------
  ;; Markdown
  ;;-----------------------------------------------------------------------------
  ;; Uses GitHub flavoured markdown for `.md` files.
  ;; TODO: make this a variable in core.
  (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))
  (add-to-list
    'auto-mode-alist
    '("\\.md.\\(erb\\|j2\\|template\\|tpl\\)\\'" . gfm-mode))

  ;; Disables auto generating code blocks when presing ` 3 times since it
  ;; behaves weirdly.
  (setq markdown-gfm-use-electric-backquote nil)

  ;; Sets indentation to 2 spaces for markdown modes.
  (setq markdown-list-indent-width 2)

  ;;-----------------------------------------------------------------------------
  ;; Nginx
  ;;-----------------------------------------------------------------------------
  (add-to-list 'auto-mode-alist '("\\.conf.\\(template\\|erb\\)\\'" . nginx-mode))

  ;;-----------------------------------------------------------------------------
  ;; Org
  ;;-----------------------------------------------------------------------------
  ;; More states avaialble for todos.
  (setq org-todo-keywords
    '((sequence "TODO" "DOING" "|" "DONE" "WONTFIX")))

  ;;-----------------------------------------------------------------------------
  ;; PHP
  ;;-----------------------------------------------------------------------------
  ;; (add-to-list 'auto-mode-alist '("\\.php.\\(template\\|erb\\)\\'" . php-mode))

  ;; (with-eval-after-load 'dap-php
  ;;   (dap-register-debug-template "PHP Debug with Docker Path"
  ;;     (list :type "php"
  ;;       :cwd nil
  ;;       :request "launch"
  ;;       :name "PHP Debug with Docker Path"
  ;;       :args '("--server=4711")
  ;;       :pathMappings (ht ("/work" (projectile-project-root (buffer-file-name))))
  ;;       :sourceMaps t)))

  ;;-----------------------------------------------------------------------------
  ;; Python
  ;;-----------------------------------------------------------------------------
  ;; Nothing here anymore :)

  ;;-----------------------------------------------------------------------------
  ;; Ruby
  ;;-----------------------------------------------------------------------------
  ;; Indentation
  (setq
    ruby-align-to-stmt-keywords      t
    ruby-align-chained-calls         nil
    ruby-deep-arglist                t
    ruby-highlight-debugger-keywords nil
    ruby-indent-level                2)

  ;;-----------------------------------------------------------------------------
  ;; Rust
  ;;-----------------------------------------------------------------------------
  ;; Indentation
  (setq rust-indent-offset 2)

  ;;-----------------------------------------------------------------------------
  ;; Restclient
  ;;-----------------------------------------------------------------------------

  (defun envchain-get (namespace variable)
    "Retrieve the value of VARIABLE in NAMESPACE from envchain.
Return the value as a string, or nil if the variable is not found."
    (let* ((env-output (string-trim
                         (shell-command-to-string
                           (format "envchain %s env" namespace))))
            (lines (split-string env-output "\n"))
            (var-prefix (format "%s=" variable))
            (matching-line (seq-find (lambda (line)
                                       (string-prefix-p var-prefix line))
                             lines)))
      (when matching-line
        (substring matching-line (length var-prefix)))))

  ;;-----------------------------------------------------------------------------
  ;; Restclient
  ;;-----------------------------------------------------------------------------
  (setq sh-basic-offset 2)
  (setq sh-indentation 2)

  ;;-----------------------------------------------------------------------------
  ;; Web (HTML CSS JS)
  ;;-----------------------------------------------------------------------------
  ;; Indentation
  (setq-default
    js-indent-level               2
    js2-basic-offset              2
    css-indent-offset             2
    web-mode-attr-indent-offset   2
    web-mode-code-indent-offset   2
    web-mode-css-indent-offset    2
    web-mode-markup-indent-offset 2)

  (with-eval-after-load 'web-mode
    (add-to-list 'web-mode-indentation-params '("lineup-args"    . nil))
    (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
    (add-to-list 'web-mode-indentation-params '("lineup-calls"   . nil)))

  ;; Remvoes warning for missing semicolons.
  (setq js2-strict-missing-semi-warning nil)

  ;;-----------------------------------------------------------------------------
  ;; Yaml
  ;;-----------------------------------------------------------------------------
  (add-to-list
    'auto-mode-alist
    '("\\.ya?ml.\\(erb\\|j2\\|template\\)\\'" . yaml-mode))

  ;;-----------------------------------------------------------------------------
  ;; Xclip
  ;;-----------------------------------------------------------------------------
  ;; Enables xclip integration for copy/paste to system clipboard.
  ;; The `xclip` package needs to be installed on the machine emacs is running.
  (xclip-mode 1)

  ;;-----------------------------------------------------------------------------
  ;; GptEL
  ;;-----------------------------------------------------------------------------
  (define-key evil-motion-state-map (kbd "\\") nil)

  (which-key-add-key-based-replacements "\\" "AI")
  (which-key-add-key-based-replacements "\\ g" "gptel")

  (evil-define-key 'normal 'global (kbd "\\ g g") 'gptel)
  (evil-define-key 'normal 'global (kbd "\\ g .") 'gptel-menu)
  (evil-define-key 'normal 'global (kbd "\\ g a") 'gptel-add)
  (evil-define-key 'normal 'global (kbd "\\ g f") 'gptel-add-file)
  (evil-define-key 'normal 'global (kbd "\\ g q") 'gptel-abort)
  (evil-define-key 'normal 'global (kbd "\\ g r") 'gptel-rewrite)
  (evil-define-key 'normal 'global (kbd "\\ g s") 'gptel-send)

  ;; Configure Claude backend
  (setq gptel-model 'claude-3-7-sonnet-20250219
    gptel-backend (gptel-make-anthropic "Claude"
                    :key (envchain-get "emacs-gptel" "CLAUDE_API_KEY")
                    :stream t))

  ;;-----------------------------------------------------------------------------
  ;; Custom
  ;;-----------------------------------------------------------------------------
  (defun eval-and-replace-region (beg end &optional region)
    "Replace the preceding sexp with its value."
    (interactive (list (mark) (point) 'region))
    (kill-region beg end region)
    (condition-case nil
      (prin1 (eval (read (current-kill 0)))
        (current-buffer))
      (error (message "Invalid expression")
        (insert (current-kill 0)))))

  (defun eval-and-replace-calc (beg end &optional region)
    "Replace the preceding sexp with its value."
    (interactive (list (mark) (point) 'region))
    (kill-region beg end region)
    (let ((result (calc-eval (substring-no-properties (current-kill 0)))))
      (condition-case nil
        (princ result (current-buffer))
        (error (message "Invalid expression")
          (insert (current-kill 0))))))

  ;; https://www.gnu.org/software/emacs/manual/html_node/calc/Function-Index.html
  (spacemacs/set-leader-keys "oc" 'eval-and-replace-calc)
  (spacemacs/set-leader-keys "or" 'eval-and-replace-region)

  ;; Setting that should be configured by default, WTF kind of math is this?
  (setq
    calc-multiplication-has-precedence nil
    calc-digit-after-point             t)

  ;;-----------------------------------------------------------------------------
  ;; Display
  ;;-----------------------------------------------------------------------------
  ;; Always display the filetree and focus the other window.
  (treemacs-select-window)
  (windmove-right)
  )


;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
