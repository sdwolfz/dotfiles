#!/usr/bin/env bash

################################################################################
# !!!WARNING!!!
################################################################################
#
# This file is used by LOGIN SHELLS, including by your Desktop Environment. That
# means everything you have in your `env` will persist between shell restarts,
# even between logouts.
#
# In order to esnure a clean state after major changes, you have to do a full
# system restart!
#
# To test changes in a completely pristine shell execute:
# ```bash
# env -i HOME="$HOME" bash -l
# ```
#
# Examples of such things to keep in mind include:
# - Removing entries from the $PATH
# - Unsetting variables or functions
# - Disabling default functionality
################################################################################

#-------------------------------------------------------------------------------
# Notes
#-------------------------------------------------------------------------------
# ColorBackground #262626

#-------------------------------------------------------------------------------
# Profiling
#-------------------------------------------------------------------------------
# [Un]comment these lines to toggle profiling.
#
# set -x
# PS4='+ $EPOCHREALTIME ($LINENO) '

#-------------------------------------------------------------------------------
# Source other scripts
#-------------------------------------------------------------------------------
# Makes sure bashrc is sourced even in a login shell.
# shellcheck source=/dev/null
[[ -f ~/.bashrc ]] && . ~/.bashrc

# Fzf completion and keybindings.
[[ -f /usr/share/fzf/completion.bash   ]] && . /usr/share/fzf/completion.bash
[[ -f /usr/share/fzf/key-bindings.bash ]] && . /usr/share/fzf/key-bindings.bash

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

# Display the exit status for the last command in the prompt.
#
# Usage:
# ```bash
# # Execute it in a shell.
# refresh_prompt
#
# # Or add it to your `PROMPT_COMMAND`.
# export PROMPT_COMMAND="refresh_prompt; $PROMPT_COMMAND"
# ```
function refresh_prompt {
  result=$?
  if [[ $result -gt 0 ]]
  then
    PS1='\[\033[00;32m\][\[\033[01;30m\]\W \[\033[01;31m\]$result\[\033[00;32m\]]\$\[\033[00m\] '
  else
    PS1='\[\033[00;32m\][\[\033[01;30m\]\W\[\033[00;32m\]]\$\[\033[00m\] '
  fi

  export PS1
}

# Enable functionality that should not be actve by default
#
# Available options:
#   - brew
#   - rbenv
#
# Usage:
#
# ```
# Enable brew rbenv
# ```
function Enable {
  case "$1" in
    "brew")
      eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
      shift
      ;;
    "rbenv")
      Enable brew

      eval "$(rbenv init - bash)"
      shift
      ;;
    *)
      return
      ;;
  esac
}

# Local helper for prepending directories to $PATH
#
# ```bash
# prepend_path ~/.local/bin
# ```
function prepend_path () {
  case ":${PATH}:" in
    *:"$1":*)
    ;;
    *)
      PATH="$1:${PATH}"
  esac
}

#-------------------------------------------------------------------------------
# Shell settings
#-------------------------------------------------------------------------------
# Enables the use of C-s for `isearch`.
stty -ixon

# Common aliases
alias ls='ls --color=auto'
alias ll='ls -la'
alias nv='nvim'
alias sp='emacs -nw'
alias spacemacs='emacs -nw'

# Quick access directories.
alias 'Dotfiles'='cd ~/Workspace/Personal/dotfiles'
alias 'Home'='cd ~'
alias 'SDW'='cd ~/Workspace/Personal/sdwolfz.gitlab.io'
alias 'Spacemacs'='cd ~/Workspace/Hub/spacemacs'
alias 'Shortcuts'='cd ~/.local/share/applications'
alias 'Workspace'='cd ~/Workspace'

# SSH without breaking `TERM`.
alias ssh="TERM=xterm ssh"

# Support for 256 colors and italic text in terminal themes and editors.
export TERM=xterm-24bit

#-------------------------------------------------------------------------------
# History
#-------------------------------------------------------------------------------
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=5000000
export HISTFILESIZE=5000000
export HISTFILE="$HOME"/.bash_history_persistent
shopt -s histappend

# Save history immediately after a command.
export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

#-------------------------------------------------------------------------------
# Android
#-------------------------------------------------------------------------------
export ANDROID_SDK_ROOT=~/snap/androidsdk/current/AndroidSDK/

#-------------------------------------------------------------------------------
# GO
#-------------------------------------------------------------------------------
export GOPATH=~/.gopath
prepend_path $GOPATH/bin

#-------------------------------------------------------------------------------
# Node and Python
#-------------------------------------------------------------------------------
prepend_path ~/.local/bin

#-------------------------------------------------------------------------------
# Homebrew
#-------------------------------------------------------------------------------
export HOMEBREW_NO_ANALYTICS=1

# Homebrew messes up the $PATH, so only use it interactively. You can enable it
# with the follwoing command (see the `Enable` function above):
#
# Enable brew

#-------------------------------------------------------------------------------
# Ruby
#-------------------------------------------------------------------------------
for entry in ~/.local/share/gem/ruby/*; do prepend_path "$entry"/bin; done

export BUNDLE_PATH=~/.local/share/gem/bundle

#-------------------------------------------------------------------------------
# Rust
#-------------------------------------------------------------------------------

prepend_path ~/.cargo/bin

#-------------------------------------------------------------------------------
# Rbenv

# This one depends on `homebrew` so enable them together with the follwoing
# command (see the `Enable` function above):

# Enable rbenv

#-------------------------------------------------------------------------------
# Cask
#-------------------------------------------------------------------------------
prepend_path ~/.cask/bin

#-------------------------------------------------------------------------------
# DOCKER
#-------------------------------------------------------------------------------
# Variables used to pass the UID and GID to `docker` or `docker-compose`.
HOST_USER_UID=$(id -u)
HOST_USER_GID=$(id -g)

export HOST_USER_UID
export HOST_USER_GID

#-------------------------------------------------------------------------------
# Prompt
#-------------------------------------------------------------------------------

export PROMPT_COMMAND="refresh_prompt; $PROMPT_COMMAND"

#-------------------------------------------------------------------------------
# Cleanup
#-------------------------------------------------------------------------------

unset prepend_path
