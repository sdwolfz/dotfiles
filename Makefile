# ------------------------------------------------------------------------------
# Settings
# ------------------------------------------------------------------------------

.DEFAULT_GOAL := help

SHELL = /bin/bash
HERE  = $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

_ASUDO = --ask-become-pass

ifdef START
	_START_AT = --start-at-task="$(START)"
endif

# ------------------------------------------------------------------------------
# Goals
# ------------------------------------------------------------------------------

.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, <GOAL_2>, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make fresh'
	@echo '  make setup clean link'
	@echo ''
	@echo 'Goals:'
	@echo '  - help: Displays this help message.'
	@echo ''
	@echo '  - ci:                Runs automated tests on this repository.'
	@echo '  - lint/ansible:      Runs `ansible-lint`.'
	@echo '  - lint/editorconfig: Runs `eclint`.'
	@echo '  - lint/shellcheck:   Runs `shellcheck`.'
	@echo '  - lint/yamllint:     Runs `yamllint`.'
	@echo ''
	@echo '  - fresh: A fresh install of the entire setup and dotfiles.'
	@echo '  - clean: Removes dotfiles or their links.'
	@echo '  - link:  Creates symbolic links for dotfiles.'
	@echo '  - setup: Installs the necessary programs and configuration.'
	@echo ''
	@echo '  - configure/docker: Configures docker.'
	@echo '  - configure/xterm:  Configures xterm.'
	@echo ''
	@echo '  - backup:  Backs up precious files and directories.'
	@echo '  - restore: Restores precious files and directories from a backup.'
	@echo ''
	@echo 'Variables:'
	@echo '  - START: Ansible task from where to start execution.'
	@echo '    * values: Any valid ansible task name.'
	@echo '    * default: Undefined'
	@echo ''
	@echo 'Default goal: help'

# ------------------------------------------------------------------------------
# Install

.PHONY: fresh
fresh: clean link setup

.PHONY: clean
clean:
	@rm -rf           \
		~/.bash_profile \
		~/.config/nvim  \
		~/.gemrc        \
		~/.gitconfig    \
		~/.npmrc        \
		~/.rubocop.yml  \
		~/.spacemacs    \
		~/.tmux.conf    \
		~/.xsession

.PHONY: link
link:
	@ln -s $(HERE)/links/lazyvim ~/.config/nvim
	@ln -s $(HERE)/.bash_profile ~/.bash_profile
	@ln -s $(HERE)/.gemrc        ~/.gemrc
	@ln -s $(HERE)/.gitconfig    ~/.gitconfig
	@ln -s $(HERE)/.npmrc        ~/.npmrc
	@ln -s $(HERE)/.rubocop.yml  ~/.rubocop.yml
	@ln -s $(HERE)/.spacemacs    ~/.spacemacs
	@ln -s $(HERE)/.tmux.conf    ~/.tmux.conf
	@ln    $(HERE)/.xsession     ~/.xsession
	@chmod +x ~/.xsession

.PHONY: setup
setup:
	@time ansible-playbook playbooks/fresh/install.yml $(_ASUDO) $(_START_AT)

# ------------------------------------------------------------------------------
# Backup

.PHONY: backup
backup:
	@time ansible-playbook playbooks/backup/save.yml $(_START_AT)

.PHONY: backup/setup
backup/setup:
	@time ansible-playbook playbooks/backup/setup.yml --ask-become-pass

.PHONY: backup/mount
backup/mount:
	@time ansible-playbook playbooks/backup/mount.yml

.PHONY: backup/check
backup/check:
	@time ansible-playbook playbooks/backup/check.yml

.PHONY: backup/sync
backup/sync:
	@time ansible-playbook playbooks/backup/sync.yml

.PHONY: backup/umount
backup/umount:
	@time ansible-playbook playbooks/backup/umount.yml

.PHONY: backup/open
backup/open:
	@xdg-open ~/Backup

.PHONY: restore
restore:
	@time ansible-playbook playbooks/backup/restore.yml

# ------------------------------------------------------------------------------
# CI
# ------------------------------------------------------------------------------

DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(HERE)":/dot -w /dot

.PHONY: ci
ci: SHELL = /bin/sh
ci: ci/ansible ci/editorconfig ci/shellcheck ci/yamllint

.PHONY: ci/ansible
ci/ansible: SHELL = /bin/sh
ci/ansible:
	@$(DOCKER_HERE) sdwolfz/ansible-lint:latest make lint/ansible

.PHONY: ci/editorconfig
ci/editorconfig: SHELL = /bin/sh
ci/editorconfig:
	@$(DOCKER_HERE) sdwolfz/editorconfig-checker:latest make lint/editorconfig

.PHONY: ci/shellcheck
ci/shellcheck: SHELL = /bin/sh
ci/shellcheck:
	@$(DOCKER_HERE) sdwolfz/shellcheck:latest make lint/shellcheck

.PHONY: ci/yamllint
ci/yamllint: SHELL = /bin/sh
ci/yamllint:
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

# ------------------------------------------------------------------------------
# Lint

.PHONY: lint/ansible
lint/ansible: SHELL = /bin/sh
lint/ansible:
	@ansible-lint -q playbooks roles variables

.PHONY: lint/editorconfig
lint/editorconfig: SHELL = /bin/sh
lint/editorconfig:
	@editorconfig-checker .

.PHONY: lint/shellcheck
lint/shellcheck: SHELL = /bin/sh
lint/shellcheck:
	@shellcheck .bash_profile

.PHONY: lint/yamllint
lint/yamllint: SHELL = /bin/sh
lint/yamllint:
	@yamllint ./.*.yml

# ------------------------------------------------------------------------------
# OS
# ------------------------------------------------------------------------------

.PHONY: os/configure
os/configure:
	@ansible-playbook playbooks/os/configure.yml --ask-become-pass

# ------------------------------------------------------------------------------
# Configure
# ------------------------------------------------------------------------------

.PHONY: configure/docker
configure/docker:
	@ansible-playbook playbooks/configure/docker.yml --ask-become-pass

.PHONY: configure/xterm
configure/xterm:
	@ansible-playbook playbooks/configure/xterm.yml --ask-become-pass

# ------------------------------------------------------------------------------
# Packages
# ------------------------------------------------------------------------------

.PHONY: packages
packages:
	@ansible-playbook playbooks/fresh/packages.yml --ask-become-pass

.PHONY: packages/cargo
packages/cargo:
	@ansible-playbook playbooks/packages/cargo.yml

.PHONY: packages/rust
packages/rust: packages/cargo

.PHONY: packages/golang
packages/golang:
	@ansible-playbook playbooks/packages/golang.yml

.PHONY: packages/brew
packages/brew: packages/homebrew

.PHONY: packages/homebrew
packages/homebrew:
	@ansible-playbook playbooks/packages/homebrew.yml

.PHONY: packages/node
packages/node:
	@ansible-playbook playbooks/packages/node.yml

.PHONY: packages/python
packages/python:
	@ansible-playbook playbooks/packages/python.yml

.PHONY: packages/ruby
packages/ruby:
	@ansible-playbook playbooks/packages/ruby.yml

.PHONY: packages/flatpak
packages/flatpak:
	@ansible-playbook playbooks/packages/flatpak.yml

.PHONY: packages/snap
packages/snap:
	@ansible-playbook playbooks/packages/snap.yml --ask-become-pass

# ------------------------------------------------------------------------------
# Source

.PHONY: packages/cask
packages/cask:
	@ansible-playbook playbooks/packages/source/cask.yml

# ------------------------------------------------------------------------------
# Emacs

.PHONY: source/emacs
source/emacs:
	@ansible-playbook playbooks/source/emacs/install.yml --ask-become-pass

.PHONY: spacemacs
spacemacs:
	@ansible-playbook playbooks/spacemacs/install.yml

# ------------------------------------------------------------------------------
# homebrew

.PHONY: brew
brew:
	@ansible-playbook playbooks/homebrew/install.yml --ask-become-pass

.PHONY: homebrew
homebrew: brew

# ------------------------------------------------------------------------------
# Other

.PHONY: docker-here
docker-here:
	@ansible-playbook playbooks/docker-here/install.yml --ask-become-pass

.PHONY: envchain
envchain:
	@ansible-playbook playbooks/envchain/install.yml --ask-become-pass

.PHONY: terraform-ls
terraform-ls:
	@ansible-playbook playbooks/terraform-ls/install.yml --ask-become-pass

# ------------------------------------------------------------------------------
# Fixes
# ------------------------------------------------------------------------------
