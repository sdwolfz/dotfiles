---

- name: HomebrewInstall
  ansible.builtin.meta: noop

- name: Clone the homebrew install repository
  ansible.builtin.git:
    repo: '{{ homebrew_clone_remote_url }}'
    dest: '{{ homebrew_repo }}'
    version: '{{ homebrew_version }}'
    remote: '{{ homebrew_clone_remote_name }}'
    clone: true
    force: true

- name: Checkout the commit sha
  ansible.builtin.command: 'git checkout {{ homebrew_sha }}'
  args:
    chdir: '{{ homebrew_repo }}'
  # Ansible Lint - There is no way to ensure the repo is clean throught the `git`
  # module.
  tags:
    - skip_ansible_lint

- name: Check if homebrew already installed
  ansible.builtin.stat:
    path: '{{ homebrew_prefix }}'
  register: homebrew_already_installed

- name: Save the repository location for sudo
  ansible.builtin.command: pwd
  register: homebrew_registered_repository_location
  args:
    chdir: '{{ homebrew_repo }}'
  when: not homebrew_already_installed.stat.exists
  changed_when: true

- name: Install homebrew
  ansible.builtin.shell: |
    set -eo pipefail

    sudo -u {{ ansible_user_id }} NONINTERACTIVE=1 `pwd`/install.sh
  args:
    chdir: '{{ homebrew_registered_repository_location.stdout }}'
  when: not homebrew_already_installed.stat.exists
  changed_when: true
  become: true

- name: Ensure nasty analytics are disabled in the env
  ansible.builtin.lineinfile:
    path: ~/.bash_profile
    line: 'export HOMEBREW_NO_ANALYTICS=1'
    state: present

- name: Disable nasty analytics from a command, then update
  ansible.builtin.shell: |
    set -eo pipefail

    eval "$({{ homebrew_prefix }}/bin/brew shellenv)"
    brew analytics off
    brew update --force
  args:
    chdir: '{{ homebrew_repo }}'
  changed_when: true
