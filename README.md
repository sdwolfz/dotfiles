# Dotfiles

Dotfiles and configurations for Manjaro.

***

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Dotfiles](#dotfiles)
    - [Fresh](#fresh)
        - [Prerequisites](#prerequisites)
        - [CI](#ci)
    - [Playbooks](#playbooks)
        - [Backup](#backup)
        - [Install](#install)
        - [Cleanup](#cleanup)
    - [Cheatsheet](#cheatsheet)
    - [Hardware](#hardware)
    - [Android](#android)
        - [Remvoe spyware](#remvoe-spyware)
    - [License](#license)

<!-- markdown-toc end -->

## Fresh

### Prerequisites

* `ansible`
* `bash`
* `git`
* `make`

```bash
sudo pacman -Syy ansible bash git make --noconfirm

git clone https://gitlab.com/sdwolfz/dotfiles.git ~/Workspace/Personal/dotfiles
cd ~/Workspace/Personal/dotfiles

make fresh
```

Or only link dotfiles:

```bash
git clone https://gitlab.com/sdwolfz/dotfiles.git ~/Workspace/Personal/dotfiles
cd ~/Workspace/Personal/dotfiles

make clean link
```

### CI

For CI you also need to install:

* `docker`
* `docker-here`

## Make

## Playbooks

### Fresh

```sh
make fresh
```

Start or continue from a specific checkpoint with:

```sh
make fresh START=SpacemacsInstall
make fresh START=SpacemacsEnd
```

#### Checkpoints

* ConfigureDocker
* ConfigureDockerEnd
* ConfigureXterm
* ConfigureXtermEnd

### Backup

First ensure the backup USB can be mounted:
```bash
ansible-playbook playbooks/backup/setup.yml --ask-become-pass
```

Then either restore or save:
```bash
ansible-playbook playbooks/backup/restore.yml
ansible-playbook playbooks/backup/save.yml
```

### Install
```bash
# Cask
ansible-playbook playbooks/cask/install.yml

# Emacs
ansible-playbook playbooks/source/emacs/install.yml --ask-become-pass

# Envchain
ansible-playbook playbooks/envchain/install.yml --ask-become-pass

# Fresh
ansible-playbook playbooks/fresh/install.yml --ask-become-pass

# LazyVim
ansible-playbook playbooks/lazyvim/install.yml

# OS
ansible-playbook playbooks/os/configure.yml --ask-become-pass
ansible-playbook playbooks/os/update.yml    --ask-become-pass
ansible-playbook playbooks/os/upgrade.yml   --ask-become-pass

# Packages
ansible-playbook playbooks/packages/install.yml --ask-become-pass

# Spacemacs
ansible-playbook playbooks/spacemacs/install.yml
```

### Configure

```sh
make configure/docker
make configure/xterm
```

## Cheatsheet

Ptrace permissions:
```bash
# Temporarly
echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope

# Permanently
echo "kernel.yama.ptrace_scope = 0" >> /etc/sysctl.d/10-ptrace.conf
```

Missing virtualbox config:
```bash
sudo modprobe vboxdrv
```

## Hardware

Diagnosis:

```bash
glxinfo
mhwd -l
mhwd -li
mhwd -lh
mhwd -la
mhwd-gpu --status
mhwd-kernel -li
```

## License

All emacs lisp code is covered by the GNU General Public License Version 3.

The rest of the code is covered by the BSD 3-clause License, see
[LICENSE](LICENSE) for more details.
